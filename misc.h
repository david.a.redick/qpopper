/*
 * Copyright (c) 1998, 1999 QUALCOMM Incorporated. All rights reserved.
 * The file license.txt specifies the considtions for use, modification,
 * and redistribution.
 */

/* Define some things that may be in different .h files on some systems */

#ifndef SEEK_SET
  #define SEEK_SET        0
  #define SEEK_CUR        1
  #define SEEK_END        2
#endif

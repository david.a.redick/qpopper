/*
 * Copyright (c) 2003 QUALCOMM Incorporated.
 *
 */

/*
 *  Banner string suffix for this POP implementation
 *
 *  If you modify qpopper, you should set this string to
 *  "Modified by foo", where 'foo' is your organization.
 *
 */

#define BANNERSFX       ""

/*
 *  IMPLEMENTATION suffix for this POP implementation
 * 
 *  If you modify qpopper, you should set this string to
 *  "modified-by-foo", where 'foo' is your organization.
 *   
 *  Note: do not use spaces in this string.  Use "-" instead.
 *
 */

#define IMPLEMENTSFX       ""



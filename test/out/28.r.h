To: "Baha'i Youth" <Bahai-Youth@BCCA.Org>
Subject: [Fwd: Arising to Serve]
Reply-To: arman1@airmail.net
X-Priority: 3 (Normal)
Date: Mon, 11 Aug 1997 15:48:55 -0700
From: Arman Hamraei <arman1@airmail.net>
X-UIDL: 0EZ!!&/&!!4m#!!~#o!!
Mime-version: 1.0
Content-Type: text/html;charset="us-ascii"

<html><pre>

--
"...Heedless souls are always seeking faults in others.
What can the hypocrite know of others' faults when he is
blind to his own? Nothing is more fruitful for man than
the knowledge of his own shortcomings... 'I wonder at the
man who does not find his own imperfections.'"
(Promulgation of Universal Peace *, page 244)

</pre></html><html><pre>
[ Included message follows: ]

</pre></html>Date: Mon, 11 Aug 1997 15:35:01 -0700
From: Arman Hamraei <arman1@airmail.net>
Reply-To: arman1@airmail.net
To: Texas Baha'is <Bahai-US-TX@BCCA.Org>
Subject: Arising to Serve
X-Priority: 3 (Normal)
Mime-version: 1.0
Content-Type: text/html;charset="us-ascii"

<HTML>
�...souls will arise and holy beings appear who, as stars, would adorn
the firmament of
<BR>divine guidance; illumine the dayspring of loving-kindness and bounty;
manifest the
<BR>signs of the unity of God; shine with light of sanctity and purity;
receive their full
<BR>measure of divine inspiration; raise high the sacred torch of faith;
stand firm as the rock
<BR>and immoveable as the mountain; and grow to become luminaries in the
heavens of His
<BR>Revelation, mighty channels of His grace, means for the bestowal of
God's bountiful care,
<BR>heralds calling forth the name of the One true God, and establishers
of the world's
<BR>supreme foundation...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
- Abdu�l-Baha

<P><B><FONT SIZE=+2>Arising to Serve</FONT></B>
<BR>When: August 22&nbsp; - Aug. 24 *
<BR>Where: North Richland Hills , Texas ( DFW area)
<BR>Cost: $25.00&nbsp; per youth for the whole weekend - All meals, book,
and place to sleep.
<BR>Limit:&nbsp; 20 youth
<BR>More info: This Ruhi course is designed to help youth teach and
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
present the Faith to there friends and others.&nbsp; Everyone
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
is strongly encouraged to attend.&nbsp; We already have
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Our beloved friend Sandy Huening will
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
be the coordinator of this event.&nbsp; Registration deadline is August
15th,
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
as we need to order books by then.
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
For more information contact Kim Krejci at 817-656-7408&nbsp;&nbsp; <A HREF="mailto:ekrejci@swbell.net">ekrejci@swbell.net</A>
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
or Arman Hamraei&nbsp; at 817-358-9306 (home),&nbsp; 800-749-1065 ext.
5341 (work)
<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<A HREF="mailto:arman1@airmail.net">arman1@airmail.net</A>

<P>What to bring: Sleeping bags, pillow, towel, personal stuff, prayer
books, pen, pencil.

<P>* If you want to attend the course and don't have rides please let me
know.&nbsp; We have people attending from different parts of Texas, so
maybe we can arrange rides.

<P>*Program will start Friday August 22 at 7:30 pm and will end Sunday
August 24, between noon and
<BR>&nbsp;3:00 pm

<P>Sponsored by the Assemblies of Colleyville and North Richland Hills.

<P>&nbsp;
<BR>"Teach ye the Cause of God, O people of Baha, for God hath prescribed
<BR>unto everyone the duty of proclaiming His Message, and regardeth it
as the
<BR>most meritorious of all deeds."

<P>--
<BR>"...Heedless souls are always seeking faults in others.
<BR>What can the hypocrite know of others' faults when he is
<BR>blind to his own? Nothing is more fruitful for man than
<BR>the knowledge of his own shortcomings... 'I wonder at the
<BR>man who does not find his own imperfections.'"
<BR>(Promulgation of Universal Peace *, page 244)
<BR>&nbsp;</HTML>

--------------EDDEDB17FE270EC20E667B78--
<html><pre>
[ vcard.vcf ]
[ attachment omitted ]

</pre></html><html><pre>
[ vcard.vcf ]
[ attachment omitted ]

</pre></html>

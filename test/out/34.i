Return-Path: <root>
Received: by sunni.qualcomm.com. (SMI-8.6/SMI-SVR4)
	id MAA28110; Fri, 1 May 1998 12:09:21 -0700
Received: from localhost (localhost) by island-resort.com (8.7.4/8.7.3) with internal id WAA15686; Sun, 20 Jul 1997 22:30:46 -0700
Date: Sun, 20 Jul 1997 22:30:46 -0700
From: Mail Delivery Subsystem <MAILER-DAEMON>
Subject: Warning: could not send message for past 4 hours
Message-Id: <199707210530.WAA15686@island-resort.com>
To: <lorenzo@java.island-resort.com>
MIME-Version: 1.0
Auto-Submitted: auto-generated (warning-timeout)
X-UIDL: fa6d34a52d3892efab3425e5a1f0d1db
Status: RO
X-Status: D
Content-Type: multipart/report; report-type=delivery-status;
	boundary="WAA15686.869463046/island-resort.com"

This is a MIME-encapsulated message

--WAA15686.869463046/island-resort.com

    **********************************************
    **      THIS IS A WARNING MESSAGE ONLY      **
    **  YOU DO NOT NEED TO RESEND YOUR MESSAGE  **
    **********************************************

The original message was received at Sun, 20 Jul 1997 17:55:18 -0700
from sumatra [219.1.1.2]

   ----- The following addresses have delivery notifications -----
<lgl@qualcomm.com>  (transient failure)

    ----- Transcript of session follows -----
 451 <lgl@qualcomm.com>... qualcomm.com: Name server timeout
 Warning: message still undelivered after 4 hours
 Will keep trying until message is 5 days old
 
--WAA15686.869463046/island-resort.com
Content-Type: message/delivery-status

Reporting-MTA: dns; island-resort.com
Arrival-Date: Sun, 20 Jul 1997 17:55:18 -0700
To: fooo

Final-Recipient: RFC822; lgl@qualcomm.com
Action: delayed
Status: 4.4.3
Last-Attempt-Date: Sun, 20 Jul 1997 22:30:46 -0700
Will-Retry-Until: Fri, 25 Jul 1997 17:55:18 -0700

--WAA15686.869463046/island-resort.com
Content-Type: message/rfc822

Return-Path: lorenzo@java.island-resort.com
Received: from sumatra.island-resort.com (sumatra [219.1.1.2]) by island-resort.com (8.7.4/8.7.3) with SMTP id RAA05084 for <lgl@qualcomm.com>; Sun, 20 Jul 1997 17:55:18 -0700
Message-Id: <3.0.1.32.19970720175316.006a00f4@java.island-resort.com>
X-Sender: lorenzo@java.island-resort.com (Unverified)
X-Mailer: Windows Eudora Pro Version 3.0.1 (32)
Date: Sun, 20 Jul 1997 17:53:16 -0700
To: lgl@qualcomm.com
From: Lorenzo <lorenzo@java.island-resort.com>
Subject: June uncle gigabyte statistics
Mime-Version: 1.0
Content-Type: multipart/mixed; boundary="=====================_869471596==_"

--=====================_869471596==_
Content-Type: text/enriched; charset="us-ascii"

<bigger>This month the stats on MTA (servers) are much improved, even
though

they will never be as accurate as the clients statistics. The high

ranking of the Zmail server is probably an example of why MTA stats

are skewed. It's being used by one of the mailing lists I track so it

is probably over-represented. I don't really know any simple way to

avoid this skew. No one that sends me mail at home or is on of the

mailing lists I track uses WorldMail yet.  WebTV seems to be tapering

off. The advertising-based free mailers continue to grow, as well as

Microsoft mailers.



                            DEC    JAN    FEB    MAR    APR    MAY   
JUN

 1 Eudora                  26.2%  25.3%  22.1%  21.2%  20.3%  22.3% 
21.0%

 2 AOL                     11.6%   9.8%  10.9%  12.0%  13.5%  15.0% 
13.9%

 3 Netscape                13.3%  13.4%  12.5%  13.8%  15.0%  14.9% 
12.7%

 4 Pine                    13.3%  12.0%  12.1%  13.1%  13.6%  11.6%  
9.8%

 5 cannot-determine         5.7%  12.1%  15.5%  10.2%   5.6%   4.1%  
5.6%

 6 MSInternetMailNews       2.7%   3.1%   2.5%   3.2%   3.8%   3.9%  
4.6%

 7 Elm                      2.9%   2.9%   2.5%   2.6%   3.2%   2.1%  
3.7%

 8 PMDF                     1.8%   1.4%   1.9%   2.9%   1.8%   3.1%  
3.6%

 9 Pegasus                  3.4%   4.6%   4.4%   4.2%   4.8%   3.1%  
3.1%

10 Juno                     0.8%   1.0%   1.3%   2.0%   2.6%   3.7%  
2.4%

11 SPRY_Mail                0.9%   0.4%   0.9%   1.3%   0.9%   0.9%  
2.0%

12 CompuServe               2.3%   1.5%   1.2%   1.2%   1.4%   2.0%  
1.6%

13 ViewMail                 0.0%   0.0%   0.1%   0.0%   0.1%   0.1%  
1.5%

14 hotmail                  0.0%   0.0%   0.0%   0.0%   0.4%   0.7%  
1.0%

15 MSExchangeServer         0.9%   0.7%   0.7%   0.6%   0.4%   0.6%  
0.9%

16 VMS                      0.7%   0.5%   1.0%   1.1%   1.4%   1.3%  
0.8%

17 ccmail                   1.4%   0.9%   1.1%   1.1%   1.1%   0.8%  
0.8%

18 Microsoft_Mail           0.3%   0.2%   0.4%   0.4%   0.2%   0.6%  
0.7%

19 Prodigy                  0.6%   0.5%   0.3%   0.4%   0.4%   0.6%  
0.7%

20 Mail*Link                0.7%   0.8%   0.7%   0.2%   0.2%   0.6%  
0.7%

21 ClarisEmailer            0.5%   0.2%   0.3%   0.4%   0.4%   0.4%  
0.6%

22 Sun-Mail-Tool            0.4%   0.3%   0.2%   0.2%   0.3%   0.5%  
0.6%

23 unknown                  0.3%   0.4%   0.2%   0.4%   0.4%   0.6%  
0.6%

24 Mutt                     0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.5%

25 Chameleon                0.7%   0.5%   0.4%   0.2%   0.6%   0.2%  
0.4%

26 MicrosoftNet             0.3%   0.5%   0.3%   0.7%   0.4%   0.6%  
0.4%

27 LotusNotes               0.0%   0.0%   0.0%   0.1%   0.4%   0.1%  
0.4%

28 GroupWise                0.3%   0.2%   0.3%   0.2%   0.3%   0.2%  
0.3%

29 InternetMailService      0.0%   0.0%   0.0%   0.1%   0.2%   0.4%  
0.3%

30 MR/2                     0.1%   0.1%   0.2%   0.2%   0.2%   0.4%  
0.3%

31 news_thingy              0.1%   0.3%   0.1%   0.2%   0.2%   0.2%  
0.3%

32 WebTV                    0.0%   0.3%   0.3%   0.9%   0.6%   0.5%  
0.3%

33 bconBBS                  0.3%   0.2%   0.2%   0.2%   0.3%   0.3%  
0.2%

34 ForteAgent               0.5%   0.4%   0.4%   0.2%   0.2%   0.4%  
0.2%

35 OutlookExpress           0.0%   0.0%   0.0%   0.0%   0.1%   0.1%  
0.2%

36 unknown-oz               0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.2%

37 Turnpike                 0.2%   0.5%   0.5%   0.4%   0.4%   0.4%  
0.2%

38 CommonLinkService        0.2%   0.3%   0.4%   0.7%   0.5%   0.2%  
0.2%

39 intel-thingy             0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.2%

40 GNU_Emacs                0.1%   0.1%   0.1%   0.2%   0.1%   0.0%  
0.2%

41 MAPI                     0.0%   0.0%   0.1%   0.3%   0.6%   0.2%  
0.2%

42 Something_on_a_VAX       0.2%   0.2%   0.1%   0.1%   0.2%   0.1%  
0.1%

43 mime-faq-poster          0.3%   0.1%   0.1%   0.1%   0.1%   0.1%  
0.1%

44 X400                     0.1%   0.2%   0.2%   0.1%   0.1%   0.1%  
0.1%

45 exmh                     0.0%   0.1%   0.1%   0.1%   0.0%   0.0%  
0.1%

46 MUSH                     0.0%   0.0%   0.0%   0.0%   0.0%   0.1%  
0.1%

47 MailBook                 0.0%   0.0%   0.0%   0.1%   0.2%   0.1%  
0.1%

48 slrn                     0.0%   0.0%   0.0%   0.0%   0.1%   0.0%  
0.1%

49 Worldtalk                0.0%   0.1%   0.0%   0.1%   0.0%   0.0%  
0.1%

50 Z_Mail                   0.2%   0.2%   0.1%   0.1%   0.1%   0.0%  
0.1%

51 E-mail_Connection        0.1%   0.3%   0.3%   0.1%   0.1%   0.1%  
0.1%

52 winATT                   0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.1%

53 libSDtMail               0.0%   0.1%   0.0%   0.1%   0.1%   0.0%  
0.1%

54 TIN                      0.1%   0.1%   0.1%   0.1%   0.1%   0.1%  
0.1%

55 UGate                    0.1%   0.1%   0.1%   0.1%   0.1%   0.1%  
0.1%

56 Vines                    0.1%   0.1%   0.0%   0.1%   0.1%   0.0%  
0.1%

57 YAM                      0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.1%

58 caldera?                 0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.1%

59 PMMail                   0.0%   0.1%   0.1%   0.1%   0.1%   0.0%  
0.1%

60 rocketmail               0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.1%

61 ZIMACS                   0.0%   0.0%   0.0%   0.0%   0.0%   0.0%  
0.1%




 EUDORA VERSIONS

                              JUN       JUN    MAY    MAY     MAY 

Rank                        # msgs  percent  #msgs  percent  rank

-----------------------------------------------------------------

  1          Win_Light_1.5.2   285    19.57    358    21.32     1

  2         Win_Pro_3.0.1_32   137     9.41    155     9.23     2

  3           Win_Pro_3.0_32   119     8.17    142     8.46     3

  4           Mac_Light_0154   112     7.69    129     7.68     4

  5       Win_Light_3.0.1_32   106     7.28    111     6.61     5

  6       Win_Light_1.5.4_16    77     5.29     63     3.75    10

  7             Mac_Pro_0212    71     4.88     54     3.22    11

  8       Win_Light_1.5.4_32    71     4.88     74     4.41     6

  9             Mac_Pro_0310    62     4.26     73     4.35     7

 10            Win_Pro_2.1.2    55     3.78     70     4.17     8

 11           Win_Pro_3.0_16    46     3.16

 12           Win_Pro_2.2_16    35     2.40

 13       Win_Light_3.0.1_16    31     2.13

 14           Mac_Light_0153    29     1.99

 15           Win_Pro_2.2_32    27     1.85

 16           Mac_Light_0302    22     1.51

 17             Mac_Pro_0300    22     1.51

 18           Mac_Light_0310    20     1.37

 19         Win_Pro_3.0.2_16    19     1.30

 20         Win_Pro_3.0.2_32    15     1.03

 21         Win_Pro_3.0.1_16    13     0.89

 22             Mac_Pro_0302    13     0.89

 23           Mac_Light_0151    13     0.89

 24    Macintosh_Pro_2.1.3-J     7     0.48

 25             Mac_Pro_0213     6     0.41

 26           Mac_Light_0110     6     0.41

 27                 <<PC_1.4>     6     0.41

 28             Mac_Pro_0214     4     0.27

 29           Mac_Light_0152     4     0.27

 30           Mac_Light_0155     3     0.21

 31           Mac_Light_0301     3     0.21

 32             Mac_Pro_0301     3     0.21

 33       Pro_for_Macintosh!     3     0.21

 34      Win_Pro_3.0.2_b4_32     3     0.21

 35      Win_Pro_3.0.3_a1_32     2     0.14

 36  Win_Light_Versisn_1.5.2     1     0.07

 37              <<PC_1.4b17>     1     0.07

 38          Win_Light_1.5.4     1     0.07

 39             Mac_Pro_0400     1     0.07

 40            -J1.3.8.5-J13     1     0.07

 41      Win_Pro_3.0.2_b2_32     1     0.07




 MTA Transfers


(Note this is much less accurate than the MUA stats because

 the MTA's closer to me get counted more frequently)


                                     JUN      JUN    MAY    MAY     MAY 

Rank                              # xfrs  percent  #xfrs  percent  rank

-----------------------------------------------------------------------

  1                       sendmail 16314    61.68   16684   58.55   1 

  2                          smail  2564     9.69    2609    9.16   2

  3                   sendmail_AIX  1837     6.95    2289    8.03   3

  4                       LISTSERV  1683     6.36    1957    6.87   4

  5                  ZmailerServer  1582     5.98    1854    6.51   5

  6                        MINTAKA   798     3.02     911    3.20   6

  7                   sendmail_Sun   233     0.88     419    1.47   7

  8                      GroupWise   196     0.74     352    1.24   8

  9                           smap   159     0.60      87    0.31  16 ^

 10                    bulk_mailer   128     0.48     114    0.40  12

 11                    IBM_VM_SMTP   109     0.41

 12                        unknown   109     0.41

 13                        Mercury   108     0.41

 14                          LMail    91     0.34

 15                           PMDF    78     0.29     159    0.56  10

 16                      queuemail    57     0.22

 17                sendmail_Ultrix    53     0.20

 18                           HTTP    50     0.19

 19                    sendmail_HP    43     0.16

 20                    post.office    41     0.16

 21                  MicrosoftMail    41     0.16

 22            MSExchangeConnector    30     0.11

 23                         NTMail    24     0.09

 24                          qmail    23     0.09

 25          IMA_Internet_Exchange    18     0.07

 26                          SMTPD    14     0.05

 27                           AIMS    13     0.05

 28                      LotusSMTP    10     0.04

 29                     EMWAC_SMTP     7     0.03

 30                           Exim     7     0.03

 31                         SLmail     6     0.02

 32               Rockliffe_SMTPRA     5     0.02

 33              InternalMailAgent     4     0.02

 34              LotusNotesGateway     3     0.01

 35           Netscape_Mail_Server     3     0.01

 36            InternetMailService     3     0.01

 37                  sendmail_OS/2     3     0.01

 38                    ccMail_Link     1     0.00

 39                           emil     1     0.00

 40                   IBM_MVS_SMTP     1     0.00

    ------------------------------ -----   ------

                             Total 26450   100.00



</bigger>
--=====================_869471596==_
Content-Type: image/gif; name="jun-muas.gif";
 x-mac-type="47494666"; x-mac-creator="4A565752"
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="jun-muas.gif"

R0lGODdhwQEVAbMAAP//////AP8A/+Pj44AAgIAAAGn//wD//wDP/wCAgAAA/wAAgAAAAAAAAAAA
AAAAACwAAAAAwQEVAQAE/xDISau9OOvNu/9gKI5kaZ5oqq5s675wLM90bd94ru987//AoHBILBqP
yKRyyWw6n9CodEqtWq/YrHbL7Xq/4LB4TC6bz+i0es1uu9/wuHxOr9vv+Lx+z+/7/4CBgoOEhYaH
iImKi4yNjo+QkZKTlJWWl5iTDJsTmwwWnp2fEqOZpqcVpZ+qFKyjq6ixsp0ArLSktbils7yWnLm4
t8Ccu73GlLDBysWrxaSe0NHS09TV1tfY2drb3N3e3+Dhv8c1ycDLrcMZDAPt7u/w8fLz9PX29/j5
+vv8/f7/+5yRY+FK2LldyQSSAsiwocOHECNKnDhA4UAVoZ7ZyrjMYi2KIP9DihxJEqTHi03YlVzJ
sqVLkidRLlH5sqbNmzjpxZSZhGbOn0CDitzJ84hPoUiTKg1YdMrRpVCjRiXalMhTqViz5qRaVchV
rWDDluTaFchXsWjTQiRb1sdZtXDjMm2bUq7du3PpzsTLt688tnp1vPVLOC3gwDgGF16s9TBiG4oZ
S4bq+DGNyJMzCz0srrPnz6C5RcGsuTROzr5Gm15NmURlQK91LGBNGynqSrFxLNhdu/dW16md7B7u
u/jL28iEDyduvDlM4DCibcg988ny2deZO9/OEHmHBSCoUlcyvoZ2edl585vNvbf3DcM/MLtFTKMu
+1TGc4SWapx/+P6kV8//ee2Z9l4G13kgXUG1mNMMOk5hRN8FDJqzDkTpYbdcgawhl+GHIBp00IgM
alReYi5YmM59ucREWoDZcWggdBwk2MF8LLZIiy3njJZig+NAWB8HL8JIoIySHYjgbvKl8swwDo7Y
YH4tOIOQiA+CIs070HDpiZebgMnOcAzs5lOX7qDZjpoVfZmmm2vC2WaYb9IZp51zniknm3zu6See
fQL6p56CFqokguHxN+UrCf2CkIpPlCfQlcro2GMqK7HXjnpILnaoI/qt2COPllpUZEScdtrXp41Q
Jx2Q9AWpikenSpSqqnaxysiJlwF1K65q6boIrzPUCtKvwIYlrCLEymBs/0jIJovVsok0G8OzIkUr
7VLUImJtdFFpuq1U3R7y7QvYkqTtuECVa8i5KWa1Lrs3uVsIvFWCNS+9LtlLCL4EibUvv2PReK2W
pIIB8Arp1jSwtOIOZXAGAlRscXijTjjGwhjB9bCqR1K0rMUXN5kwpF5wnELDOKUaMa7XPTfCTiRX
jPHJl36hMgos58RbyDIKKPHME9Rs9NFICyDqK07uLJhqeMUMM4gmTYyB0fIpOp/TOXBdQs8+xxg0
pwRuuJbVF2B9o5NSen2D2zPzFXOGvoX88jtmA+SvgmzDaiLcB0MB9k9Abyr0PncvVTjieeuztyCA
hzA44UY2Hs/ihH/cj//lOqH9SOThIUm31EhhPhHneHsOKtQQi+1r4i1ZHp8IoJNRu4Lszk06S6a/
9CHtwUVKL9Cun665TelJHrwTk5cG++W723N370LZiDFuDPsXJH5TVkohwQAVjzfZx1fPJPDYr7yj
QRXmDAr4GOoevVrgEZ0+QVKiw7SL8B8rPlyPCwSvoiQMRrnvff2jyPwAqLpWVQlCENTFpLZUJ0JZ
UEwYrGAG73RBDXqQgxvMUwgD1UEyGWqEg0LhCT+YpwaegFJ9sx3+IkiqhFwogcfyS7kCkCgReU8M
oSrg0tLBNAw0D4eF2eHNghGKjOxHe/ub1e1iaIJX7Yd7B6EVEoHVrQD/eNFkObJhGEm0Px96hXVb
RBJyvMjGNrrxjXCcUDOaKAof8scVijLCFImURlVRy4180xGONCbEEvUEjX0s0B/bGMhFrY9EjyRG
GX94RsElUo0ulEAcibQiJzJKinWEBRQPaclLciiAPdgjJ0tpyvagkgeqnA4iW9mcV/ohljek5XZs
2QdcGlGX3OElH3z5PWA6R5h7ICYCjWkcZOpBmZhiZjMzyaxZSrND1KyWNa85I/slh5XcpI0z8wDN
VoTTPdmcmRPdUM5OnLM24xTiG9q5kHdi05sbOMAE9Lm2++SRIwrbpj0Z0y198rOfgnzkAbVAz48M
tJvoA8ABJkrRilr0/6IXFYWjyAjQlAn0oYQpqESbRESFbuyjYiHAAFSqUpDCg1oHPagsSzrGMEiq
FR19ohnNyRcC+NSl8Yin9wjYUY9iBIZDnKQGjniTluIlACMxmswiqgkJJayQOcqlXXz606dGtWIB
qNhUlXc/9ZmobwbcCVNbwtWVutUubBSJxcIqgLFe75sSAsUQiTjBaKiwgyBkIQkz6FM1FXaFgU2s
CAWbQg3C8bFwBGybwOpFAQyWsYZKp7lmGMMaquOXcHHqPESbFcjyA7JvpCxdazYRodrhpjRk32eL
iZau2oO0SXHjO3T7EJK1QwBwfAdrG+LaOsCWe59cmhZTitt6NPcmvP+NR3D/4VvptrEdXpSHVPlR
XDo0dK0Uee5tc3LdklR3HtMdQFzpMdx5qO2uVQWnVGzbD/H6A6r1iC5JLBaR9dajvb+tGVUl8d2s
0Pcf9j2tdf1rXv6GJLv5SJrNyIpX5q0Ev/w4MEM03I+4lre3B7DYAbTr4AtDWB/vJWmFm4DhkbQ4
HwlGMEP0G5GKhTjE7jivTRj8XwFTmAm0opBmG8kEfUJ1xCLhsXNjDBAmuwO1EqGoiAEcFCXDI8VE
rs46hFxJJxwgAEjWB2rHfOLROnnD9vhwO77sxTDjA6MWzbEAbjxnNyulzPOgFgEAsOc9K2iUQPok
KIs6pCY6aqOiXOf/P7eX5SWA+dFktnJ+r/tGd3A4JKSl8ZMfzWZ3wLmi9yBZiG285olGRdIVSSdX
VRylErVar45kER6fVEZXD1gJnAYzqPur5k0TANXh7fU8Pm1nFJd4u542NVSUzENvcvXZ0I62tH1q
RzqO0ZD6u+Pf2ubPHeVRxUVmo513DZD0Wlq05k5zmq976QFcdM3uBoiO+UHRU7e42bfOwKqzbOt+
2wfbRZRnrU2a7yOkO9n1lki7d6vpJ8tD2O1o6bvjEeZi91isEUn4nQFZcAv0mc+NbBQkrw3rIZVK
oUUUJco7joQXxRnN/6i0erPbcHdXlAAWb8i8KaLx3H6R5TzTnqWG/57c7uEUSgb020a6zURGI9TC
/3g5jM+MD0iDWR4Tf0eMkX3lErdE2ULhOHwJjFJ9kDvibm0rRXJt87PTI8Fc33lNeo4TsYOb7PJ9
SJyfvQ9iY7TNbZYxe5MGFbq7ZJN3j0Q5uT6RieLc71mveq5fvA/xMl4qhm9Jd+eweCpPRO0hqXhD
uirhuqIl4aIfyebl0Hm5Q4Slb8WKxQhgejm7XiugzrlEVh+H1ntd4VnhOtXVkvnWDvleTiB8RIYv
b893/ffxYL5YRix14+MT70xA9u1h3JLLO9+5jEl9260P9GEJdPujdUnpay941rj9H7yHQ4FDDX15
SJ8hl3fI/fFS/P98xH+enOU3ODUrO+VO+1B/aLdfpQcSC6cZ73cProUjWsNlVTRUQmYhrpI98qQ/
WQVa/aBj+3dx0Jd/r5d2xdF/QZVNBbCCBZA1oURTVGQ/QmdERpeB6sMjMGRD/KNzFYNbJPh86PcS
oGccKLgm1MSCLPhnL9g0hlZ0iTZKhlZHSpdQSAdBRVeA3uRZTbdQPKV/A1Bd+feDSBGCi/GAqXZ9
GICEK8g3Kmd0RDdbWeJvCfVqbfhqJ6eFeZU/etgifXVZirUJBHAmSMMARuOHi/WHjYWIgDKEhtiI
iXiIkOiIiDUnCccAB8AJMtVIariJnNiJLdhtj0JygUZAfGhtKif/aFT4hIhGdFbCWXh4JQG3TE02
eN8XFrDXgERYUZaIhhewiWw4dOvTPnsFjOpwisFYjDljjKJCSV+zgXooRrTVfiQWhGlBhqUxUSqY
hIGkVFR4cjX1hikna8oYa/mDg8vIMwMYKzm4XNI4jQi4VcByiZnYaAzDRGcFjk4ocIf2WWnVjVXo
jDlVdm/HHbjoGyP2CfO4SisGZO3oHNaYJMf3LwIZfUjykJ4SkYMwf5WnKhYZUhgJOROpdbhSkPdU
ftWUd+OVLB2JF//HTiEZeyrpSh8pQCG5kqVhk3HRkm2gkSlJLyQ5GTrJBjw5kPCDk2gRlGswlPaH
Q0apLDMJGyjV/5S18ZMeyYtHZW1C+VFS6RtbORVPuVQxiAZKaWmm1JXcolkJMHaqeAZjCZOJZJZJ
US5pmTV0VGhmMJZwaRxUmSs0kgB++ZeAGZiCKZhYwo93OUt56ZAQ9WMcMJf8ZphlMJSJuR2TWS9o
qZaQKUPgVJmUqRkRqG3hGJmsw5kESZosgZRqMH+m2R6raVeJBwnt1GduaU+tOTQmqU1PMIQg9VOw
B48fx5iKdy05NYNcyGdABQ+6GRf7BpywGThohYwakJwuJZ21RW1WuTroQoOyxoWyWZsc0lZ8Z4vg
aZ3M+TnREZBbqFbHSZa9GXHU+RPhiXa/OXbBWSzDKEEUQkGKKP9Zksifj9iffxWgmMUACFCgBooA
ehKeADqgGPRse4KE1+lA9gmQsyWLIHWgB3pb8TkS0GYPvlie2PkjleJZsRhNIYGhBbodKGqg8ZCh
+LChDwGjHqqGt+ktwrkRsiKFHggSK9qjPaoPKwoUPooA/sCiU7eXWvee9cCJNUo768RXUOR0VIKS
DIGiVTqkWJqlLooPQeqjJ2qkLzqESChaMioPnlgAA/Ch9FmP+ElrhMQFbbkPGKoWWjqkFIGE9gCm
YUoALGgAK1imadqJ9wChTUphWUKOxMhQL9kOVppGm8ilejqjBjCpaBqogsoQjgmiF6AAnKoACGWM
GBhocCqQc0r/S4+qDwVgoGfap5NKqZUKEpl6V506q7Raq7bKqRo1cKHaUPRIHvNQqtuBpxKxqsS6
gu8Qqe/QqsoaEn45q5r6Qk3jjyiDBeWUpREhrHJxqvlArO7AgglgrFdaoMo6ruRqAA5xq7i6pnlo
j29YnFPKBEF6p2poF9pqqZ64Dyv4rQ0xrnraqvGwrPGArrOaAPAQq6+5rob5TzoTBQQrEt4KrnCx
qhAxmPtArr96oAYAprQ6rpwaD4DZoij6rObnBPn6qteqht86r8PKggxxppY6q/8wmH6ZrO0gmPAw
rnJapxjKrzp7oCJ7kkxQADI7tERLtEIrs8WKqiq7pJdKDwqw/4IwSw9Eew/++q8Aqw+0GrMzi7P1
YABfeUskW7Imy634KrQmOwBF65dka6kp2w4uiw+16g4NS6s2+w/lerU1G5jyELX+8LHzgLft0KoR
uitha7ZLKxENyw9pu4kpC7H2AJi1mrYz2w5x2w93uw/Nyrf94Lf6cLeTWqjI1wT1KhZ/ebSS+5e2
2rlwq7kD4LmBC7hOy7d6Kw+Jm7eTu6/k+rO4KbosexclOw+V6w9V27Wuaw/Du7esOw91S7CcGxHj
CroSSaVwIbAQwbXFC7xZ+6/I27ENMbshIbjQm5FRkLxaQb0Tcb0Mkb23ShG1KxJeO7gj+wTmixMC
26nayxCea/+urQu7EBG8sLoSqJkGcVq/64u93DsABGy5k4oP6EsPx5ssASyWi2oP6GoA/vsQ1tvA
lksvnymlAhwF+jsSD4y/+TvCAxWBWHiYT2DCzsu/JXy5VsvCJ/y12vmPsfZEAXeF5LHC/OvAL/zD
MqzAIbyey7KinzpyJmeH7brDTQDEMNy5Pey868klNNKzWJqr24aj+ygk5WQthEacFsG137vAU6x5
NEyBFviNcHiOLRcdsrWdJxHE1VvG/XLGsIZVN2yHSQxkbjyikeSuQzwUADzIMEHIrtmrZpWObprH
BKjDpCSiGZOeN2LIqkfJIQFeaQLAdtyMJ5WdfmyBThcaojz/yqRcyqZ8yuGwyYniwaPqyZGcMSXK
xq7sLOVwGbVsy/ALtA/0ybDMjO6ayLSMy8E8ocSsuzZ6nlqsXGApzMPczH1czM5pzJtlGcfAq+9K
zbxgzRGCzb2gzVLgzdzcygsZzqYAzoKTEljEptD8zNHczuzMyeNcBDmcIqy8MvUcdN9SVFVyzy/E
z8vszFmpZVeFjm64y/4sg82SdJ580OjD0GGZAe8rzTbFx3sIzPoMrRoFyQK4rhddgRm9y+n8dBsA
vkskQYBmVDPxKiAN0LP8zi7d0tn0vCUNjil8zeR8CqXgxC+sV3L4y9+cEiq9z99GEEFN1EN9lR3d
z0eN1A7t/8sVINOrpMSkKM6PHMsEXaF5aNUYjdVXrdUezdVd7dQ1zAEkHdVQMnI+7SNaBtZb7dXw
7NY0AtcRJddzLdZZaNd33AERrYRumswLu9Z0bah4LdhpzWqDbdiFHXKHjdhsYc6RAtSB7aSRLTkb
hS6VLdSLvcqZTZebndihSx7w0tROehnnItqUTdpXHV9AltT4nM+srdRe/NpVJNtfQ9t5jX2REtq6
jdrOstvF0tjLc9PljMrEXdykjAmOnaum7RVFLdyQs9xD0IrbLMvO3dvSQM/XbdCxnd3qXNOS3dyp
3dnV/dUsvdK2ra7dLd4zBdDJPQv4Utpv89tLjbCWPd/jff/f+K14c3SeG23U3j3a5+2CAS7g0L1U
+53fuDzQGK3gXz3Z2/jfJMXgnCzh0EHhCE7QFt7QEA5u0+rRGX49Hw7iG/7g6n3hIu3ZCjniIl3g
aFziLY7i663iKe7iJh7jCU3d4U0sSNXdNy7jT9fescDiROLbvf3bvF3kNf42A77Kri3k67Dkf+bk
RgTlUQ7k1Uzkwnnk/G3kXJ7kXv7lt/Qf+SHm+9zfGu2KUs42VA7mNezg0f2meRXiVW7fE+7jyyzn
bG7jMG7g3M3je75lFR3WC23ngI7neQ7oOhqA75zmP6TjHd7Pgb7ghH7o59jjzqzjOG7PSJ7Q4E3p
nv7poB5L6qI+6qSen31e6qjeNam+6mbB6q7+6rAe67I+67Re67Z+67ie67q+67ze677+68Ae7MI+
7MRe7MZ+7Mie7Mq+7Mze7M7+7NDuCBEAADs=
--=====================_869471596==_
Content-Type: text/enriched; charset="us-ascii"




--=====================_869471596==_--


--WAA15686.869463046/island-resort.com--





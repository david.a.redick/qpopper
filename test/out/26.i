Return-Path: <root>
Received: by sunni.qualcomm.com. (SMI-8.6/SMI-SVR4)
	id LAA27964; Fri, 1 May 1998 11:38:45 -0700
Received: from bcca.org (bounty.bcca.org [208.133.44.40])
	by booray.new-era.com (8.8.5/8.8.5) with SMTP id RAA24905
	for <lgl@island-resort.com>; Wed, 10 Sep 1997 17:13:00 -0400
Received: by bcca.org (Smail3.1.29.1 #3)
	id m0x8tYa-0003qPC; Wed, 10 Sep 97 16:40 EDT
		  to Bahai-Discuss-Request@BCCA.Org
X-Loop: Bahai-Discuss-request@Bounty.BCCA.Org
Errors-To: Bahai-Discuss-x@Bounty.BCCA.Org
Sender: Bahai-Discuss-x@Bounty.BCCA.Org
X-IMPORTANT-NOTE: Please send unsubscribe messages
		  to Bahai-Discuss-Request@BCCA.Org
To: "Baha'i Discuss" <Bahai-Discuss@BCCA.Org>
References: <01INH3ADGE2QDBQ3PV@saturn.gov.bc.ca>
Mime-Version: 1.0
Message-Id: <3417041C.349E278F@accessone.com>
Subject: Re: Idea for non-Baha'is at Feasts
X-Mailer: Mozilla 4.0 [en] (Win95; I)
Reply-To: jershaw@accessone.com
Organization: Terran Technologies
X-Priority: 3 (Normal)
Date: Wed, 10 Sep 1997 13:33:33 -0700
From: "Jerome A. Shaw" <jershaw@accessone.com>
Content-Transfer-Encoding: 7bit
Content-Type: text/html; charset=us-ascii
X-UIDL: ]XPe9f'J!!I1#"!im]d9
Status: RO

<HTML>
Glen Little wrote:
<BLOCKQUOTE TYPE=CITE>At recent Feasts, there have been a few non-Baha'is
who joined us
<BR>for the spiritual time, but went out during the consultation.&nbsp;
I think
<BR>this could (and should) become more common in the future.</BLOCKQUOTE>
Glen,How is this idea rectified with the following quote:

<P>&nbsp;Let him remember the example set by Abdu'l-Baha, and His constant
admonition to shower such kindness upon the seeker, and exemplify to such
a degree the spirit of the teachings he hopes to instill into him, that
the recipient will be spontaneously impelled to identify himself with the
Cause embodying such teachings.&nbsp; Let him refrain, at the outset, from
insisting on such laws and observances as might impose too severe a strain
on the seeker's newly awakened faith, and endeavor to nurse him, patiently,
tactfully, and yet determinedly, into full maturity, and <B>aid him to
proclaim his unqualified acceptance of whatever has been ordained by Baha'u'llah.&nbsp;
Let him, as soon as that stage has been attained, introduce him to the
body of his fellow-believers</B>, and seek, through constant fellowship
and active participation in the local activities of his community, to enable
him to contribute his share to the enrichment of its life, the furtherance
of its tasks, the consolidations of its interests, and the coordination
of its activities with those of its sister communities.&nbsp; Let him not
be content until he has infused into his spiritual child so deep a longing
as to impel him to arise independently, in his turn, and devote his energies
to the quickening of other souls, and the upholding of the laws and principles
laid down by his newly adopted Faith.
<BR>(Shoghi Effendi:&nbsp; The Advent of Divine Justice, page 52)

<P>Just curious about your take on this?

<P>- Jerome</HTML>




